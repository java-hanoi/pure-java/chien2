public class Chien {
	static enum SituationChien{PLACE,ABONDONNE,FOURIERE};

	static int nbPattes = 4;

	String raceChien; //Stocke la race du chien
	String nomChien; //Stocke le nom du chien
	int ageChien; //Stocke l'age du chien
	SituationChien situationChien;

	//Constructeur avec paramètres
	public Chien(String race, String nom, int age,SituationChien situation) {
	    System.out.println("le chien "+ nom +" est de race "+ race +" il a "+ age +" ans\nCe chien est "+Chien.SituationChien.ABONDONNE);
    	raceChien = race;
	    nomChien = nom;
  	    ageChien = age;
	    situationChien = situation;
	}  

	public static void main(String []args) {
		Chien leChien = new Chien("caniche", "toto", 5, Chien.SituationChien.ABONDONNE);
	}
}